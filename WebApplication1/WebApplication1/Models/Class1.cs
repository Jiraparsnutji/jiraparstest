﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;

namespace WebApplication1.Models
{
    public class UserRegistration
    {
        MongoClient _client;
        MongoServer _server;
        MongoDatabase _db;

        public UserRegistration()
        {
            _client = new MongoClient("http://localhost:27017");
            _server = _client.GetServer();
            _db = _server.GetDatabase("JiraparsTest");
        }
        public Users addUser(Users user)
        {
            _db.GetCollection<Users>("Users").Save(user);
            return user;
        }

    }
}